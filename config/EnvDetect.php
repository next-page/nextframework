<?php

namespace M2bFramework\config;

class EnvDetect {
    
    public static function process() {
        try {

            $oCache = \Zend\Cache\StorageFactory::factory(array(
            'adapter' => array(
                'name' => 'filesystem',
                'options' => array(
                    'cache_dir' => CACHE_PATH,
                    'ttl' => 1000
                )
            ),
            'plugins' => array('serializer'))
            );

            if ($oCache->hasItem('envconfig_xml')) {
                $aConfig = $oCache->getItem('envconfig_xml');
            } else {
                $oConfig = new \Zend\Config\Reader\Xml();
                $aConfig = $oConfig->fromFile(CONFIG_PATH . DIRECTORY_SEPARATOR . 'envconfig.xml');
                $aConfig = $aConfig['configs'];
                $oCache->setItem('envconfig_xml', $aConfig);
            }

            $sHttpHost = str_replace('www.', '', str_replace(':', '_', $_SERVER['HTTP_HOST']));
            if (!empty($aConfig) && array_key_exists($sHttpHost, $aConfig)) {
                    define('APPLICATION_ENV', $aConfig[$sHttpHost]['environment']);
            }

            defined('APPLICATION_ENV')
                || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


        } catch(Exception $e) {
            \M2bFramework\Log\Logger::getInstance()->log('Application ENV not detected.');
        }
    }
}
?>
