<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'log' => array(
        'Log\App' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 7,
                    'options' => array(
                        'stream' => 'data/logs/app.log',
                    ),
                ),
            ),
        ),
        ),
        'view_manager' => array(
            'display_not_found_reason' => false,
            'display_exceptions' => false,
            'doctype' => 'HTML5',
            'not_found_template' => 'error/404',
            'exception_template' => 'error/index',
            'template_map' => array(
                'layout/layout' =>  M2B_MODULE_PATH.'/Defualt/view/layout/layout.phtml',
                'error/404' =>  M2B_MODULE_PATH.'/Defualt/view/error/404.phtml',
                'error/index' =>  M2B_MODULE_PATH.'/Defualt/view/error/index.phtml',
            ),
            'template_path_stack' => array(
                  '/../view',
                  M2B_MODULE_PATH.'/Defualt/view',
            ),
        ),
        // Placeholder for console routes
        'console' => array(
            'router' => array(
                'routes' => array(
                ),
            ),
        ),
    
);
