<?php

namespace NextFramework\Log;

class Logger extends \Zend\Log\Logger {
    const defualtFileName = 'error.log';

    static $_oInstance = null;


    public static function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
            self::$_oInstance->addWriter(self::$_oInstance->getDefaultWriter());
        }

        return self::$_oInstance;

    }

    private function getDefaultWriter() {
        return new \Zend\Log\Writer\Stream(self::getCurrentFileLogPath());
    }
    
    static public function getCurrentFileLogPath($fileName = null, $basePath = '') {
        $dir = $basePath.DIRECTORY_SEPARATOR.date('Y-m-d');
        
        if($fileName === null) {
            $fileName = self::defualtFileName;
        }
        if(!is_dir($dir)) {
            mkdir($dir, 777);
        }

        return $dir.DIRECTORY_SEPARATOR.$fileName;
    }
   
}
?>
