<?php

namespace NextFramework\Mvc;

class Application extends \Zend\Mvc\Application {

    /** 
     * Defualt envirement
     * @var string
     */
    const defualtEnv = 'production';
    /**
     * Envirement
     * @var string
     */
    public static $env;
    /*
     * global config path
     * @var string
     */
    public static $configGlobalPath;
    /*
     * Local config path
     * @var string
     */
    public static $configLocalPath = '/config';
    
    /**
     * Config file prefix
     * @var string
     */
    public static $configAppFileName = 'application.config';
    
    /**
     * Config file extension
     * @var string
     */
    public static $configAppExt = 'php';

    public function __construct($configuration, ServiceManager $serviceManager) {
        return parent::__construct($configuration, $serviceManager);
    }

    /**
     * Set path for global config
     * @param string $configPath
     */
    public static function setGlobalConfigPath($configPath) {
        static::$configGlobalPath = $configPath;
    }
    
    /**
     * Set path for local config
     * @param string $configPath
     */
    public static function setLocalConfigPath($configPath) {
        static::$configLocalPath = $configPath;
    }

    /**
     * Init Framework
     * @param array $configuration
     * @param string $env
     */
    public static function initApp() {
             
        return parent::init(self::initConfig());
    }
    
    /**
     * Build configuration
     */
    public static function initConfig(){
        
        $globalConfigFile = static::$configGlobalPath.'/'.static::$configAppFileName.'.'.static::$configAppExt;
        $localConfigFileEnv = static::$configLocalPath.'/'.static::$configAppFileName.'.'.self::getEnvirement().'.'.static::$configAppExt;
        $globalConfigFileEnv = static::$configGlobalPath.'/'.static::$configAppFileName.'.'.self::getEnvirement().'.'.static::$configAppExt;
        
        if (is_readable($globalConfigFile)) {
            $configuration = include_once $globalConfigFile;
        }
        
        if (is_readable($globalConfigFileEnv)) {
            $configurationEnv = include_once $globalConfigFileEnv;
        }
         
        if (is_readable($localConfigFileEnv)) {
            $localEnv = include_once $localConfigFileEnv;
        }
        
        if (isset($localEnv) && is_array($localEnv)) {
                $configurationEnv = array_replace_recursive($configurationEnv, $localEnv);
            
        }
        if (isset($configurationEnv) && is_array($configurationEnv)) {
                $configuration = array_replace_recursive($configuration, $configurationEnv);
            
        }
        return $configuration;
    }

   /**
    * Get Envirement
    * @return string
    */
    public static function getEnvirement() {
        if(is_null(self::$env)) {
            $appEnv = APPLICATION_ENV;
            static::$env = $appEnv ? $appEnv : self::defualtEnv;
        }
        return static::$env;
        
    }

}
