<?php
namespace NextFramework\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AbstractController extends AbstractActionController
{


    protected $mapper;

    protected $options;

    /**
     * Gets mapper
     */
    protected function getMapper()
    {
        return $this->userMapper;
    }

    protected function getOptions()
    {
        return $this->options;
    }
}
