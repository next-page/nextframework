<?php

namespace NextFramework\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

class AbstractMapper {

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * 
     */
    protected $options;
    protected $query;
    protected $queryBuilder;
    protected $entity;
    protected $where;
    protected $hydratorMode = \Doctrine\ORM\Query::HYDRATE_OBJECT;

    public function __construct(EntityManagerInterface $em, $options= null) {
        $this->em = $em;
        $this->options = $options;
    }

    protected function createQueryBuilder($columns = 'o') {
        $er = $this->em->getRepository($this->entity);
        return $this->queryBuilder = $er->createQueryBuilder($columns);
    }
    
    public function getRepository($entity){
        $this->setEntity($entity);
        return $this->em->getRepository($this->entity);       
    }



    protected function execute() {
        $this->prepareQuery();
        return $this->query->execute();
    }

    protected function prepareQuery() {
        $this->query = $this->queryBuilder->getQuery();
        $this->query->setHydrationMode($this->getHydratorMode());
    }

    protected function persist($entity) {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function getHydratorMode() {
        return $this->hydratorMode;
    }

    public function setHydratorMode($hydrator) {
        if (!empty($hydrator)) {
            $this->hydratorMode = $hydrator;
        }
    }
    
    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    public function getQueryBuilder() {
        return $this->queryBuilder;
    }

    public function setQueryBuilder($queryBuilder) {
        $this->queryBuilder = $queryBuilder;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }
    
    public function getEntityManager() {
        return $this->em;
    }

    public function setEntityManager(\Doctrine\ORM\EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function getOptions() {
        return $this->options;
    }

    public function setOptions($options) {
        $this->options = $options;
    }





}
