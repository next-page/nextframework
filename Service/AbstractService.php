<?php

namespace NextFramework\Service;

use NextFramework\EventManager\EventProvider;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class AbstractService extends EventProvider implements ServiceManagerAwareInterface {

    protected $mapper;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var UserServiceOptionsInterface
     */
    protected $options;

    /**
     * @var Hydrator\ClassMethods
     */
    protected $hydrator;
    
    protected $doctrineHydratorMode;
    
    protected $authService;
    
    protected $dataIn;

    public function getMapper() {
        $this->mapper->setHydratorMode($this->getDoctrineHydratorMode());
        return $this->mapper;
    }

    public function setMapper($mapper) {
        $this->mapper = $mapper;
        return $this;
    }

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return object
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getHydrator() {
        if (!$this->hydrator instanceof Hydrator\HydratorInterface) {
            $this->setHydrator($this->getServiceManager()->get('nextShop_product_hydrator'));
        }

        return $this->hydrator;
    }

    public function setHydrator(Hydrator\HydratorInterface $hydrator) {
        $this->hydrator = $hydrator;
        return $this;
    }
    
    public function getDoctrineHydratorMode() {
        return $this->doctrineHydratorMode;
    }

    public function setDoctrineHydratorMode($doctrineHydratorMode) {
        $this->doctrineHydratorMode = $doctrineHydratorMode;
    }
    
    /**
     * getAuthService
     *
     * @return AuthenticationService
     */
    public function getAuthService()
    {
        return $this->authService;
    }

    /**
     * setAuthenticationService
     *
     * @param AuthenticationService $authService
     * @return User
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
        return $this;
    }
    
    public function getDataIn($name = null) {
        $result = null;
        if(is_null($name)) {
            $result =  $this->dataIn;
        }
        
        if(isset($this->dataIn[$name])) {
            $result =  $this->dataIn[$name];
        }
        
        return $result;
        
    }

    public function setDataIn($dataIn) {
        $this->dataIn = $dataIn;
    }





}
