<?php

namespace NextFramework;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use NextFramework\Service\ErrorHandling as ErrorHandlingService;
use NextFramework\Log\Logger;
use Zend\Log\Writer\Stream as LogWriterStream;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;
use Zend\Validator\AbstractValidator;

class Module implements
AutoloaderProviderInterface, ConfigProviderInterface {

    protected $logPath;
    protected $eventManager;
    public static $langObj;

    public function onBootstrap(MvcEvent $e) {
        $this->eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($this->eventManager);
        //$this->initSession($e);
        //$this->initLogger($this->eventManager);
        $this->initTranslator($e);
        //$eventManager->attach('dispatch', array($this, 'loadConfiguration'));
    }

    protected function initLogger($eventManager) {

        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, function($event) {
                    $exception = $event->getResult()->exception;
                    if ($exception) {
                        $sm = $event->getApplication()->getServiceManager();
                        $service = $sm->get('\NextFramework\Service\ErrorHandling');
                        $service->logException($exception);
                    }
                });
    }

    public function initSession($e) {

        $serviceManager = $e->getApplication()->getServiceManager();
        $config = $serviceManager->get('Config');
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session']);
        $session = new SessionManager();
        $session->start();

        $container = new Container('initialized');
        if (!isset($container->init)) {

            $request = $serviceManager->get('Request');

            $session->regenerateId(true);
            $container->init = 1;
            $container->remoteAddr = $request->getServer()->get('REMOTE_ADDR');
            $container->httpUserAgent = $request->getServer()->get('HTTP_USER_AGENT');


            if (!isset($config['session'])) {
                return;
            }

            $sessionConfig = $config['session'];
            if (isset($sessionConfig['validators'])) {
                $chain = $session->getValidatorChain();

                foreach ($sessionConfig['validators'] as $validator) {
                    switch ($validator) {
                        case 'Zend\Session\Validator\HttpUserAgent':
                            $validator = new $validator($container->httpUserAgent);
                            break;
                        case 'Zend\Session\Validator\RemoteAddr':
                            $validator = new $validator($container->remoteAddr);
                            break;
                        default:
                            $validator = new $validator();
                    }

                    $chain->attach('session.validate', array($validator, 'isValid'));
                }
            }
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'NextFramework\Service\ErrorHandling' => function($sm) {
                    $logger = $sm->get('NextFramework\Log');
                    $service = new ErrorHandlingService($logger);
                    return $service;
                },
                'NextFramework\Log' => function ($sm) {
                    $log = new Logger();
                    $writer = new LogWriterStream(\NextFramework\Log\Logger::getCurrentFileLogPath(null, $this->getLogPath()));
                    $log->addWriter($writer);

                    return $log;
                },
            ),
        );
    }

    public function getLogPath() {
        if (!$this->logPath) {
            $this->logPath = ROOT_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'log';
        }
        return $this->logPath;
    }

    public function setLogPath($logPath) {
        $this->logPath = $logPath;
    }

    public function loadConfiguration(MvcEvent $e) {
        
    }

    protected function initTranslator(MvcEvent $event) {
        if (is_null(self::$langObj)) {
            $serviceManager = $event->getApplication()->getServiceManager();
            $langService = $serviceManager->get('language_service');
                    
            self::$langObj = $langService->detect();
            $translator = $serviceManager->get('translator');
            
            $translator
                    ->setLocale(self::$langObj->locale)
                    ->setFallbackLocale('en_US');
            AbstractValidator::setDefaultTranslator($translator);
        }
        return self::$langObj->locale;
    }

    protected function addTranslationToView($langObj) {
        $this->eventManager->attach(
                MvcEvent::EVENT_RENDER, function($e) use ($langObj) {
                    $viewModel = $e->getViewModel();
                    $viewModel->locale = $langObj->locale;
                    $viewModel->language = $langObj->lang;
                }, 100
        );
    }

    /**
     * Get info about route
     * @param type $sm
     * @return \stdClass
     */
    protected function getRouteInfo($sm) {
        $router = $sm->get('router');
        $matchedRoute = $this->getRouteMatch($sm);
        if ($matchedRoute) {
            return $this->parserRoute($matchedRoute);
        }
        return false;
    }

    protected function getRouteMatch($sm) {
        $router = $sm->get('router');
        $request = $sm->get('request');
        return $router->match($request);
    }

    protected function parserRoute($route) {

        $params = $route->getParams();
        if (isset($params)) {
            $result = new \stdClass;
            $controller = $params['controller'];
            $module_array = explode('\\', $controller);
            $result->module = isset($module_array[0]) ? $module_array[0] : '';
            $result->controllerName = isset($module_array[2]) ? $module_array[2] : '';
            $result->controller = $controller;
            $result->action = isset($params['action']) ? $params['action'] : '';
            $result->name = $route->getMatchedRouteName();
            return $result;
        }
        return false;
    }

    /**
     * Check match route is the same
     * @param type $name
     * @param type $sm
     * @return boolean
     */
    public function isModule($name, $sm) {
        $route = $this->getRouteInfo($sm);
        if ($route && $route->module === $name) {
            return true;
        }
        return false;
    }

}
